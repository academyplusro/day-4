package ro.orangetraining.meciuritest;

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println("Incepe programul");
        Schi partiePeDeal = new Schi();

        Meci<Schi> unMeciCiudatDeSchi = new Meci<Schi>(partiePeDeal);
     //echivalent cu
          /*
public class Meci {
    Schi tipMeci;
    Meci(Schi meci){
        System.out.println("Incepe meciul");
        tipMeci = meci;
        System.out.println(tipMeci.getClass());
    }
}
*/
        Patinaj patinajPeLacESuperb = new Patinaj();
        Meci<Patinaj> unMeciDePatinajSpecial = new Meci<Patinaj>( patinajPeLacESuperb) ;
        //echivalent cu
   /*
public class Meci {
    Patinaj tipMeci;
    Meci(Patinaj meci){
        System.out.println("Incepe meciul");
        tipMeci = meci;
        System.out.println(tipMeci.getClass());
    }
}
*/
        //pentru a filtra tipurile de obiecte cu care lucreaza o clasa
        Fotbal arsenalSauReal = new Fotbal();
        MeciuriCuMingi<Fotbal> meciSpectaculosSpaniol = new MeciuriCuMingi<>(arsenalSauReal);
        //Eroare : Schi nu extinde MeciuriCuMingi
        //        MeciuriCuMingi<Schi> meciuriCuMingiSiSchi = new MeciuriCuMingi<Schi>(unMeciCiudatDeSchi);

    }
}
