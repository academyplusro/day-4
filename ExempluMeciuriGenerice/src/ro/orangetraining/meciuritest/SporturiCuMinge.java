package ro.orangetraining.meciuritest;

public class SporturiCuMinge implements Sport {
    @Override
    public void efortFizic() {
        System.out.println("Ai grija sa nu ametesti alergand dupa minge");
    }

    @Override
    public int scor() {
        return 1;
    }
}
