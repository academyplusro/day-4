package ro.orangetraining.varargs;

public class Main {

    public static void main(String[] args) {
        try{
            int suma = sum(2,5, 35);
            System.out.println(suma);

            System.out.println(args[5]);
            throw new Exception("o alta eroare ilustrativa");
        }
        catch( ArrayIndexOutOfBoundsException e){
            System.out.println("Te-am prins!");
            //e.printStackTrace();
        }
        catch( Exception e){
            System.out.println("Te-am prins, dar nu sunt sigur");
            //e.printStackTrace();
        }
        finally {
            //ruleaza indiferent ce
            System.out.println("Ruleaza oricum");
        }

    }

    public static int sum(Integer... numbers){
        Integer total = 0;
        for(Integer integ : numbers){
            total = total + integ;
        }
        return total;
    }

}
